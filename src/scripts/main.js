$(document).ready(function() {

  $('.js-toForm').on('click', function (e) {
      e.preventDefault();
      $('html, body').animate({
          scrollTop: $('.contacts').offset().top
      }, 500);
  });
  
  $('.js-phone').mask('+0 (000)-000-00-00');

});
